use cognizant;

-- Table: Course
DROP TABLE IF EXISTS Student;
DROP TABLE Course;
CREATE TABLE Course
(
    CourseId    INT          NOT NULL,
    Title       VARCHAR(200) NULL,
    Fees        FLOAT        NULL,
    Description VARCHAR(45)  NULL,
    Trainer     VARCHAR(100) NULL,
    Start_Date  DATE         NULL,
    PRIMARY KEY (CourseId)
);

-- Table: Student
DROP TABLE IF EXISTS Student;
CREATE TABLE Student
(
    EnrollmentId INT NOT NULL,
    StudentId    INT NULL,
    CourseId     INT NULL,
    PRIMARY KEY (EnrollmentId)
);

-- Table: Trainer
DROP TABLE Trainer;
CREATE TABLE Trainer
(
    TrainerId INT         NOT NULL,
    Password  VARCHAR(45) NULL,
    PRIMARY KEY (TrainerId)
);

INSERT INTO Course
VALUES (101, 'Python', 350.0, 'python course', 'Colt Steel', '2021-03-13'),
       (102, 'NodeJs', 400.0, 'nodejs course', 'Alan Jades', '2019-05-04'),
       (103, 'React', 600.0, 'react course', 'Steve Rogers', '2020-06-12'),
       (104, 'AWS', 550.0, 'aws course', 'Stephen Grider', '2021-02-21');

INSERT INTO Student(EnrollmentId, StudentId, CourseId)
VALUES (5001, 222, 101),
       (5043, 333, 102),
       (5034, 444, 103),
       (5056, 555, 104);

INSERT INTO Trainer (TrainerId, Password) VALUE (123, 'pass');

SELECT *
FROM Trainer;
SELECT *
FROM Student;
DESC Student;