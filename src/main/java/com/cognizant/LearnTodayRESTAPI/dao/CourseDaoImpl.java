package com.cognizant.LearnTodayRESTAPI.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.cognizant.LearnTodayRESTAPI.model.Course;

@Repository
public class CourseDaoImpl implements CourseDao {

    @Autowired
    JdbcTemplate template;

    @Override
    public List<Course> getAllCourses() {

        return template.query("select * from Course", new ResultSetExtractor<List<Course>>() {
            @Override
            public List<Course> extractData(ResultSet rs) throws SQLException, DataAccessException {

                List<Course> list = new ArrayList<Course>();
                while (rs.next()) {
                    Course course = new Course();
                    course.setCourseId(rs.getInt(1));
                    course.setTitle(rs.getString(2));
                    course.setFees(rs.getFloat(3));
                    course.setDescription(rs.getString(4));
                    course.setTrainer(rs.getString(5));
                    course.setStartDate(rs.getDate(6));
                    list.add(course);
                }
                return list;
            }
        });
    }

    @Override
    public Course getCourseById(int id) {
        List<Course> list = getAllCourses();
        for (Course course : list) {
            if (course.getCourseId() == id) {
                return course;
            }
        }
        return null;
    }
}
