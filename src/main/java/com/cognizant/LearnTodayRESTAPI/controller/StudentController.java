package com.cognizant.LearnTodayRESTAPI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.LearnTodayRESTAPI.model.Student;
import com.cognizant.LearnTodayRESTAPI.service.StudentService;

@RestController
@RequestMapping("/api")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/Student")
    public ResponseEntity<Object> getAllCourses() {
        return new ResponseEntity<>(studentService.getAllCourses(), HttpStatus.OK);
    }

    // add a student
    @PostMapping("/Student")
    public ResponseEntity<Object> postStudent(@RequestBody Student student) {
        System.out.println(student);
        try {
            if (studentService.postStudent(student)) {
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    // delete a student
    @DeleteMapping("/Student/{id}")
    public ResponseEntity<Object> deleteStudentEnrollment(@PathVariable int id) {
        if (studentService.deleteStudent(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Enrollment information not found", HttpStatus.NOT_FOUND);
    }
}
