package com.cognizant.LearnTodayRESTAPI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.LearnTodayRESTAPI.model.Trainer;
import com.cognizant.LearnTodayRESTAPI.service.TrainerService;

@RestController
@RequestMapping("/api/")
public class TrainerController {

    @Autowired
    TrainerService trainerService;

    // create a new trainer
    @PostMapping("/Trainer")
    public ResponseEntity<Object> trainerSignUp(@RequestBody Trainer t) {
        boolean signUp = trainerService.trainerSignUp(t);
        if (signUp) {
            return new ResponseEntity<>(t, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    // update password of trainer
    @PutMapping("/Trainer/{id}")
    public ResponseEntity<Object> updatePassword(@PathVariable int id, @RequestBody Trainer t) {
        String response = trainerService.updatePassword(id, t);
        if (response.equals("updated")) {
            return new ResponseEntity<>("Data updated successfully", HttpStatus.OK);
        }
        if (response.equals("notfound")) {
            return new ResponseEntity<>("Searched Data Not Found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
